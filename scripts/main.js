// Selektieren von Element von Originaltext und Kopie
const elOrigText = document.getElementById('original-text');
const elTextCopy = document.getElementById('text-copy');
// Kopieren von Originaltext über Property innerHtml
// Property textContent darf nicht verwendet werden, da dieses
// die html-Struktur nicht übernimmt, resp.
elTextCopy.innerHTML = elOrigText.innerHTML;

// Titel von kopierten Text selektieren und anpassen
const elTitleCopy = elTextCopy.getElementsByTagName('h2')[0];
elTitleCopy.textContent =
  'Kopierter Text mit hervorgehobenen, wichtigen Begriffen';

// Elemente mit wichtigen Begriffe selektieren
const elImportantTerms = elTextCopy.getElementsByTagName('span');
// Selektierten Elementen Klasse important hinzufügen
for (let i = 0; i < elImportantTerms.length; i++) {
  const elImportantTerm = elImportantTerms[i];
  elImportantTerm.classList.add('important');
}
